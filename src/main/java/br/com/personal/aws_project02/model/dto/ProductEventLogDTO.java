package br.com.personal.aws_project02.model.dto;

import br.com.personal.aws_project02.enums.EventType;
import br.com.personal.aws_project02.model.ProductEventLog;

public class ProductEventLogDTO {

    private final String code;
    private final EventType eventType;
    private final Long productId;
    private final String username;
    private final Long timestamp;
    private final String messageId;

    public ProductEventLogDTO(ProductEventLog productEventLog) {
        this.code = productEventLog.getPk();
        this.eventType = productEventLog.getEventType();
        this.productId = productEventLog.getProductId();
        this.username = productEventLog.getUsername();
        this.timestamp = productEventLog.getTimestamp();
        this.messageId = productEventLog.getMessageId();
    }

    public String getCode() {
        return code;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Long getProductId() {
        return productId;
    }

    public String getUsername() {
        return username;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getMessageId() {
        return messageId;
    }
}
